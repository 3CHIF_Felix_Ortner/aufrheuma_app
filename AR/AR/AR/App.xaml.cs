﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AR
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();
            var tabbedPage = new TabbedPage();
            tabbedPage.SelectedTabColor = Xamarin.Forms.Color.Teal;
            tabbedPage.UnselectedTabColor = Xamarin.Forms.Color.DarkTurquoise;
            tabbedPage.BarBackgroundColor = Xamarin.Forms.Color.PaleTurquoise;
            tabbedPage.Children.Add(new Page1_Login());
            tabbedPage.Children.Add(new Page2_Studie());
            tabbedPage.Children.Add(new Page3_Bericht());
            tabbedPage.Children.Add(new Page4_Chat());

            MainPage = tabbedPage;
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
